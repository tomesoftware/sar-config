#!/bin/bash

echo "UPDATING"
echo "Beginning SAR update process..." > /mnt/data/updater.txt
echo -e "Update log for `date '+%Y-%m-%d @ %H:%M:%S'`\n" >> /mnt/data/updater.txt

/usr/bin/wget -q --spider http://google.com

if [ $? -eq 0 ]; then
    REVERSE_DNS_HOSTNAME="$(dig -x `dig TXT +short o-o.myaddr.l.google.com @ns1.google.com | tr -d '"'` +short)"

    echo -e "Reverse DNS: $REVERSE_DNS_HOSTNAME\n" >> /mnt/data/updater.txt 2>&1

    if [[ $REVERSE_DNS_HOSTNAME == *"mobile"* ]]; then
        echo "Detected LTE connection. Git/Docker pull deferred.\n" >> /mnt/data/updater.txt 2>&1
    else
        echo "Online. Updating git repo..." >> /mnt/data/updater.txt
        /usr/bin/git pull origin $DEPLOYMENT_GIT_BRANCH >> /mnt/data/updater.txt 2>&1
        /usr/bin/git checkout $DEPLOYMENT_GIT_BRANCH >> /mnt/data/updater.txt 2>&1
        echo -e "" >> /mnt/data/updater.txt

        echo "Pulling docker images..." >> /mnt/data/updater.txt
        /usr/local/bin/docker-compose --project-name sar pull >> /mnt/data/updater.txt 2>&1
        echo -e "" >> /mnt/data/updater.txt
    fi
else
    echo -e "Offline. Skipping repo/docker pull.\n" >> /mnt/data/updater.txt
fi

echo "Applying docker-compose.yml..." >> /mnt/data/updater.txt
/usr/local/bin/docker-compose --project-name sar up -d --remove-orphans >> /mnt/data/updater.txt 2>&1
echo -e "" >> /mnt/data/updater.txt

echo "Cleaning up docker resources..." >> /mnt/data/updater.txt
/usr/bin/docker images --quiet --filter=dangling=true | xargs -r /usr/bin/docker rmi >> /mnt/data/updater.txt 2>&1
/usr/bin/docker volume ls --quiet --filter=dangling=true | xargs -r /usr/bin/docker volume rm >> /mnt/data/updater.txt 2>&1
echo -e "" >> /mnt/data/updater.txt

echo "Sleeping for 5 minutes..." >> /mnt/data/updater.txt
echo "SLEEPING"
sleep 300
